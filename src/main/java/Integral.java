import java.text.DecimalFormat;
import java.util.Scanner;

public class Integral {
    public static double IterationMethod(double a, double b, double e) {

        boolean negativeIntegral = false;

        if (e < 0){
            throw new RuntimeException("e must be positive");
        }

        if (a > b) {
            double temp = a;
            a = b;
            b = temp;
            negativeIntegral = true;
        }

        if (a == b) {
            return 0;
        }

        double result = 0;
        int n = (int)Math.round(Math.sqrt(2*Math.pow(b-a, 3)/(24*e))) + 1;
        double d = (b - a)/ n;

        for (int i = 0; i < n; i++) {
            double x = a + d * (i + 0.5);
            result += x*x;
        }

        result *= d;

        if (negativeIntegral) {
            return -result;
        } else {
            return result;
        }
    }

    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
//        System.out.println("Enter A: ");
//        double a = sc.nextDouble();
//        System.out.println("Enter B: ");
//        double b = sc.nextDouble();
//        System.out.println("Enter e: ");
//        double e = sc.nextDouble();

        System.out.println(IterationMethod(1, 2, 0.0001));
    }
}

