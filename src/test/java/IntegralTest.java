import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IntegralTest {
    @Test
    public void aIsGreaterThanBIntegralIsNegative() {

        assertTrue(Integral.IterationMethod(3, 1, 0.01) < 0);

    }

    @Test
    public void bIsGreaterThanAIntegralIsPositive() {
        assertTrue(Integral.IterationMethod(1, 2, 0.01) > 0);
    }

    @Test
    public void aEqualsBResultIsZero() {
        assertEquals(Integral.IterationMethod(1, 1, 0.01), 0);
    }

    @Test
    public void eIsNegative() {
        Exception exception = assertThrows(RuntimeException.class, () -> Integral.IterationMethod(1, 2, -2));
        String expectedMessage = "e must be positive";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void checkIfIntegralIsCorrect() {
        double expectedE = 0.001;
        double actualE = Math.abs((Integral.IterationMethod(0,2, expectedE) - 8.0/3));
        assertTrue(expectedE > actualE);
    }




}
